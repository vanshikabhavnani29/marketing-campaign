CREATE TABLE response
(
customer_id number(10) references bank_client_data(customer_id),
response varchar2(10)
);

select count(*) from response;
--delete from response where 1;
--truncate table response;

CREATE TABLE postal
(
customer_id number(10) references bank_client_data(customer_id),
postal_code number(10)
);

CREATE TABLE social_economic
(
customer_id number(10) references bank_client_data(customer_id),
emp_var_rate number(10,10),
cons_price_idx number(10,10),
cons_conf_idx number(10,10),
euribor3m number(10,10),
nr_employed number(10,10)
);

select count(*) from social_economic;

CREATE TABLE customer_campaign
(
customer_id number(10) references bank_client_data(customer_id),
contact varchar2(20),
mon varchar2(20),
day_of_week varchar2(20),
durations number(10),
campaign number(10),
pdays number(10),
previous number(10),
poutcome varchar2(20)
);