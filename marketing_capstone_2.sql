select * from region;

--SELECT DISTINCT region_code FROM customer WHERE region_code NOT IN (
--    SELECT region_code FROM region);
--    
--UPDATE customer SET region_code = NULL WHERE region_code = 0;

select count(*) from campaign;

select count(*) from response;

SELECT COUNT(CUSTOMER_ID) AS "CUSTOMER ID", AGE, JOB
FROM CUSTOMER
GROUP BY AGE, JOB
ORDER BY AGE;

SELECT CUSTOMER_ID, AGE, JOB
FROM CUSTOMER
GROUP BY AGE, JOB, CUSTOMER_ID
ORDER BY AGE; 

SELECT CUSTOMER_ID FROM CUSTOMER
WHERE housing = 'no' and loan = 'no';

SELECT COUNT(CUSTOMER_ID) AS "CUSTOMER ID", HOUSING, LOAN FROM CUSTOMER
WHERE housing = 'no' and loan = 'no'
GROUP BY HOUSING, LOAN;

SELECT count(CUSTOMER_ID) FROM CUSTOMER
WHERE housing = 'no' and loan = 'no';

select defaults from customer;

--SELECT customer_id from customer where defaults = 'yes' and customer_id in 
--(select customer_id from campaign where count(campaign) > 2);

select customer_id from campaign where customer_id in (SELECT customer_id from customer where defaults = 'yes');

select * from campaign where customer_id = 19413;

select count(customer_id), defaults from customer
group by defaults;

select customer_id from campaign 
where (campaign > 2)
and (customer_id in (SELECT customer_id from customer where defaults = 'yes'));

select count(customer.customer_id) AS "CUSTOMER ID", state_name from customer 
inner join state on customer.state_code = state.state_code
group by state_name
order by count(customer_id);

select count(customer.customer_id) AS "CUSTOMER ID", state_name from customer 
inner join state on customer.state_code = state.state_code
group by state_name
order by count(customer_id) desc;

select count(customer.customer_id) AS "CUSTOMER ID", state_name from customer 
inner join state on customer.state_code = state.state_code
group by state_name
having count(customer_id) IN (SELECT min(count(customer_id)) from customer inner join state on customer.state_code = state.state_code
group by state_name);

select count(customer.customer_id) AS "CUSTOMER ID", state_name from customer 
inner join state on customer.state_code = state.state_code
group by state_name
having count(customer_id) IN (SELECT max(count(customer_id)) from customer inner join state on customer.state_code = state.state_code
group by state_name);

select count(customer.customer_id) AS "CUSTOMER ID", city_name from customer 
inner join city on customer.city_code = city.city_code
group by city_name
order by city_name;

select count(customer.customer_id) AS "CUSTOMER ID", city_name from customer 
inner join city on customer.city_code = city.city_code
group by city_name
having count(customer_id) IN (SELECT max(count(customer_id)) from customer inner join city on customer.city_code = city.city_code
group by city_name);

select count(customer.customer_id) AS "CUSTOMER ID", city_name from customer 
inner join city on customer.city_code = city.city_code
group by city_name
having count(customer_id) IN (SELECT min(count(customer_id)) from customer inner join city on customer.city_code = city.city_code
group by city_name);

select count(customer.customer_id) AS "CUSTOMER ID", city_name from customer 
inner join city on customer.city_code = city.city_code
group by city_name 
order by city_name desc;


select count(customer.customer_id) AS "CUSTOMER ID", region_name from customer 
inner join region on customer.region_code = region.region_code
group by region_name
order by region_name;

select count(customer.customer_id) AS "CUSTOMER ID", region_name from customer 
inner join region on customer.region_code = region.region_code
group by region_name 
order by region_name desc;

select count(customer.customer_id) AS "CUSTOMER ID", region_name from customer 
inner join region on customer.region_code = region.region_code
group by region_name
having count(customer_id) IN (SELECT min(count(customer_id)) from customer inner join region on customer.region_code = region.region_code
group by region_name);

select count(customer.customer_id) AS "CUSTOMER ID", region_name from customer 
inner join region on customer.region_code = region.region_code
group by region_name
having count(customer_id) IN (SELECT max(count(customer_id)) from customer inner join region on customer.region_code = region.region_code
group by region_name);