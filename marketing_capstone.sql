CREATE TABLE region
(
region_code number(1) primary key,
region_name varchar2(10)
);

select count(*) from bank_client_data;
delete from BANK_CLIENT_DATA where 1;

CREATE TABLE states
(
state_code varchar2(5) primary key,
state_name varchar2(20),
region_code number(1) references region(region_code)
);

--drop table states;

CREATE TABLE city
(
city_code varchar2(5) primary key,
city_name varchar2(20),
state_code varchar2(5) references states(state_code)
);

DROP TABLE bank_client_data;

--ALTER TABLE state MODIFY STATE_CODE varchar2(2);

CREATE TABLE bank_client_data
(
customer_id number(10) primary key,
age number(3),
jobs varchar2(255),
marital varchar2(20),
education varchar2(255),
default_credit varchar2(10),
housing varchar2(10),
loan varchar2(10),
region_code number(1) references region(region_code),
state_code varchar2(5) references states(state_code),
city_code varchar2(5) references city(city_code)
);

select count(*) from res_temp;